plugins {
    `java-library`

    id("io.freefair.lombok") version "8.6"
}

group = "io.github.ecies"
version = "0.0.1"

repositories {
    mavenCentral()
}

dependencies {
    implementation(platform("org.junit:junit-bom:5.10.2"))

    implementation("org.bouncycastle:bcprov-jdk15to18:1.78.1")

    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.withType<Test> {
    useJUnitPlatform()
}
