package ecies;

import ecies.common.ECKeyPair;
import lombok.SneakyThrows;
import org.bouncycastle.jce.interfaces.ECPrivateKey;
import org.bouncycastle.jce.interfaces.ECPublicKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;

class EciesTest {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private static final String MESSAGE = "Helloworld_Helloworld_Helloworld_Helloworld_Helloworld";

    @BeforeAll
    static void setup() {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Test
    void encryptDecryptBinaryTest() {
        ECKeyPair ecKeyPair = generateEcKeyPair();
        byte[] encrypted = Ecies.encrypt(ecKeyPair.getPublicBinary(true), MESSAGE.getBytes(UTF_8));
        String decrypted = new String(Ecies.decrypt(ecKeyPair.getPrivate(), encrypted), UTF_8);
        assertEquals(MESSAGE, decrypted);
    }

    /**
     * Generates new key pair consists of {@link ECPublicKey} and {@link ECPrivateKey}
     *
     * @return new EC key pair
     */
    @SneakyThrows
    static ECKeyPair generateEcKeyPair() {
        KeyPairGenerator g = KeyPairGenerator.getInstance("EC", new BouncyCastleProvider());
        g.initialize(Ecies.EC_SPEC, SECURE_RANDOM);
        KeyPair keyPair = g.generateKeyPair();
        return new ECKeyPair((ECPublicKey) keyPair.getPublic(), (ECPrivateKey) keyPair.getPrivate());
    }
}
