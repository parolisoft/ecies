package ecies;

import ecies.common.AESGCMBlockCipher;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.val;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.DerivationFunction;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.generators.HKDFBytesGenerator;
import org.bouncycastle.crypto.params.HKDFParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.ECPointUtil;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;

import javax.crypto.KeyAgreement;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import static org.bouncycastle.util.Arrays.concatenate;

@SuppressWarnings("TypeMayBeWeakened")
@UtilityClass
public class Ecies {

    private static final String CURVE_NAME = "secp256k1";
    private static final int UNCOMPRESSED_PUBLIC_KEY_SIZE = 65;
    private static final int AES_IV_LENGTH = 16;
    private static final int AES_TAG_LENGTH = 16;
    private static final int AES_IV_PLUS_TAG_LENGTH = AES_IV_LENGTH + AES_TAG_LENGTH;
    private static final int SECRET_KEY_LENGTH = 32;
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private static final String KEY_AGREEMENT_ALGORITHM = "ECDH";

    // VisibleForTesting
    static final ECNamedCurveParameterSpec EC_SPEC = ECNamedCurveTable.getParameterSpec(CURVE_NAME);

    private static final ECNamedCurveSpec curvedParams =
            new ECNamedCurveSpec(CURVE_NAME, EC_SPEC.getCurve(), EC_SPEC.getG(), EC_SPEC.getN());


    /**
     * Encrypts given message with given public key
     *
     * @param receiverPubKeyBytes EC public key binary
     * @param message message to encrypt binary
     * @return encrypted message binary
     */
    @SneakyThrows
    public static byte[] encrypt(byte[] receiverPubKeyBytes, byte[] message) {
        KeyPair pair = generateEphemeralKey(EC_SPEC);

        val ephemeralPrivKey = (org.bouncycastle.jce.interfaces.ECPrivateKey) pair.getPrivate();
        val ephemeralPubKey = (org.bouncycastle.jce.interfaces.ECPublicKey) pair.getPublic();

        // generate receiver PK
        KeyFactory keyFactory = getKeyFactory();
        PublicKey receiverPubKey = getEcPublicKey(receiverPubKeyBytes, keyFactory);

        // Derive shared secret
        byte[] aesKey = hkdf(keyAgreementSecret(KEY_AGREEMENT_ALGORITHM, ephemeralPrivKey, receiverPubKey));

        // AES encryption
        return aesEncrypt(message, ephemeralPubKey, aesKey);
    }

    private static byte[] keyAgreementSecret(String algorithm, PrivateKey ephemeralPrivKey, PublicKey receiverPubKey)
        throws GeneralSecurityException {
        KeyAgreement keyAgreement = KeyAgreement.getInstance(algorithm, BouncyCastleProvider.PROVIDER_NAME);
        keyAgreement.init(ephemeralPrivKey);
        keyAgreement.doPhase(receiverPubKey, true);
        return keyAgreement.generateSecret();
    }

    /**
     * Decrypts given ciphertext with given private key
     *
     * @param privateKey EC private key
     * @param cipherBytes cipher text binary
     * @return decrypted message binary
     */
    @SneakyThrows
    public static byte[] decrypt(PrivateKey privateKey, byte[] cipherBytes) {
        KeyFactory keyFactory = getKeyFactory();

        // get sender pub key
        byte[] senderPubKeyBytes = Arrays.copyOf(cipherBytes, UNCOMPRESSED_PUBLIC_KEY_SIZE);
        PublicKey senderPubKey = getEcPublicKey(senderPubKeyBytes, keyFactory);

        // decapsulate
        byte[] aesKey = hkdf(keyAgreementSecret(KEY_AGREEMENT_ALGORITHM, privateKey, senderPubKey));

        // AES decryption
        byte[] encrypted = Arrays.copyOfRange(cipherBytes, UNCOMPRESSED_PUBLIC_KEY_SIZE, cipherBytes.length);
        return aesDecrypt(encrypted, aesKey);
    }

    private static KeyFactory getKeyFactory() throws GeneralSecurityException {
        return KeyFactory.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
    }

    private static byte[] aesEncrypt(byte[] message, org.bouncycastle.jce.interfaces.ECPublicKey ephemeralPubKey, byte[] aesKey)
            throws InvalidCipherTextException {
        AESGCMBlockCipher aesgcmBlockCipher = new AESGCMBlockCipher();
        byte[] nonce = new byte[AES_IV_LENGTH];
        SECURE_RANDOM.nextBytes(nonce);

        CipherParameters parametersWithIV = new ParametersWithIV(new KeyParameter(aesKey), nonce);
        aesgcmBlockCipher.init(true, parametersWithIV);

        int outputSize = aesgcmBlockCipher.getOutputSize(message.length);

        byte[] encrypted = new byte[outputSize];
        int pos = aesgcmBlockCipher.processBytes(message, 0, message.length, encrypted, 0);
        aesgcmBlockCipher.doFinal(encrypted, pos);

        byte[] tag = Arrays.copyOfRange(encrypted, encrypted.length - nonce.length, encrypted.length);
        encrypted = Arrays.copyOfRange(encrypted, 0, encrypted.length - tag.length);

        byte[] ephemeralPkUncompressed = ephemeralPubKey.getQ().getEncoded(false);
        return concatenate(ephemeralPkUncompressed, nonce, tag, encrypted);
    }

    private KeyPair generateEphemeralKey(AlgorithmParameterSpec ecSpec)
            throws Exception {
        KeyPairGenerator g = KeyPairGenerator.getInstance("EC", BouncyCastleProvider.PROVIDER_NAME);
        g.initialize(ecSpec, SECURE_RANDOM);
        return g.generateKeyPair();
    }

    private static byte[] aesDecrypt(byte[] encrypted, byte[] aesKey) throws InvalidCipherTextException {
        byte[] nonce = Arrays.copyOf(encrypted, AES_IV_LENGTH);
        byte[] tag = Arrays.copyOfRange(encrypted, AES_IV_LENGTH, AES_IV_PLUS_TAG_LENGTH);
        byte[] ciphered = Arrays.copyOfRange(encrypted, AES_IV_PLUS_TAG_LENGTH, encrypted.length);

        AESGCMBlockCipher aesgcmBlockCipher = new AESGCMBlockCipher();
        CipherParameters parametersWithIV = new ParametersWithIV(new KeyParameter(aesKey), nonce);
        aesgcmBlockCipher.init(false, parametersWithIV);

        int outputSize = aesgcmBlockCipher.getOutputSize(ciphered.length + tag.length);
        byte[] decrypted = new byte[outputSize];
        int pos = aesgcmBlockCipher.processBytes(ciphered, 0, ciphered.length, decrypted, 0);
        pos += aesgcmBlockCipher.processBytes(tag, 0, tag.length, decrypted, pos);
        aesgcmBlockCipher.doFinal(decrypted, pos);
        return decrypted;
    }

    private static byte[] hkdf(byte[] master) {
        DerivationFunction hkdfBytesGenerator = new HKDFBytesGenerator(new SHA256Digest());
        hkdfBytesGenerator.init(new HKDFParameters(master, null, null));
        byte[] aesKey = new byte[SECRET_KEY_LENGTH];
        hkdfBytesGenerator.generateBytes(aesKey, 0, aesKey.length);
        return aesKey;
    }

    private static PublicKey getEcPublicKey(byte[] senderPubKeyByte, KeyFactory keyFactory)
            throws InvalidKeySpecException {
        ECPoint point = ECPointUtil.decodePoint(curvedParams.getCurve(), senderPubKeyByte);
        KeySpec pubKeySpec = new ECPublicKeySpec(point, curvedParams);
        return keyFactory.generatePublic(pubKeySpec);
    }
}
